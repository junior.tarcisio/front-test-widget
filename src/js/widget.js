if (typeof Widget != "undefined")
    throw "namespace conflict: Widget";

window.addEventListener('scroll', function(e) {
    document.body.style.backgroundPositionY = window.pageYOffset*1.1 + "px";
})

var Widget = {};

(function() {
    

    /*
     * Css 
     * at upper classe for singleton load
     */
    Widget.css = {};
    Widget.css.loaded = false;
    Widget.css.load = function(callback) {
        if (Widget.css.loaded) 
            return;

        var css = document.createElement("link");
        css.setAttribute("type", "text/css");
        css.setAttribute("rel", "stylesheet");
        css.setAttribute("href", "css/widget.css");
        css.onload = callback;
        document.getElementsByTagName("head")[0].appendChild(css);
        Widget.css.loaded = true;
    };

    
    /*
     * Template 
     * at upper classe for singleton load
     */
    Widget.template = {}
    Widget.template.body = null;
    Widget.template.loadTemplateHttp = function(callback) {
        var http = new XMLHttpRequest();

        http.onload = function() {
            console.log(this.responseText);
            callback(this.responseText);
        }

        http.open("GET", "widget.html", true);
        http.send();
    }

    Widget.template.getTemplate = function(callback) {
        if (Widget.template.body != null) {
            callback(Widget.template.body);
            return;
        }

        Widget.template.loadTemplateHttp(function(template) {
            Widget.template.body = template;
            callback(Widget.template.body)
        });        
    }

    
    /*
     * CallbackWidget object 
     * can be instanced N times
     */
    Widget.CallbackWidget = function (args) {   
        this.args = args;
        this.container = document.createElement('div');
        this.controls = {};
        this.closed = false;
        var self = this;

        if (!args) args = {};
        if (!args.horizontal) args.horizontal = "right";
        if (!args.vertical) args.vertical = "bottom";

        Widget.css.load();
        
        Widget.template.getTemplate(function(template){
            self.container.innerHTML = template;
            document.body.appendChild(self.container);
    
            self.controls = new Widget.Controls(self);
            self.message = new Widget.Message(
                self.controls.message, 
                self.controls.messageHolder);
            Widget.validations.setListeners(self.container);

            self.setPosition(self.controls.panel, args);
            self.setPosition(self.controls.fab, args);
        });
    }

    Widget.CallbackWidget.prototype.setPosition = function(control) {
        if (this.args.horizontal == "left") {
            control.style.left = "16px";
            control.style.right = "auto";
        }
        else {
            control.style.left = "auto";
            control.style.right = "16px";
        }
        
        if (this.args.vertical == "top") {
            control.style.top = "16px";
            control.style.bottom = "auto";
        }
        else {
            control.style.top = "auto";
            control.style.bottom = "16px";
        }
    }


    /*
     * form
     */
    Widget.Controls = function(widget) {

        this.fab = widget.container.getElementsByClassName("widget-fab")[0];
        this.panel = widget.container.getElementsByClassName("widget")[0];
        this.closeButton = widget.container.getElementsByClassName("widget__button-close")[0];
        this.name = widget.container.getElementsByClassName("name")[0];
        this.phone = widget.container.getElementsByClassName("phone")[0];
        this.interest = widget.container.getElementsByClassName("interest")[0];
        this.email = widget.container.getElementsByClassName("email")[0];
        this.callButton = widget.container.getElementsByClassName("widget__button-call")[0];
        this.spinnerCall = widget.container.getElementsByClassName("spinner-container")[0];
        this.message = widget.container.getElementsByClassName("widget__message")[0];
        this.messageHolder = widget.container.getElementsByClassName("widget__message__holder")[0];
        this.name.focus();

        this.closeButton.onclick = function() {
            widget.toggle();
        } 

        this.fab.onclick = function() {
            widget.toggle();
        } 

        this.callButton.onclick = function() {
            widget.call();
        } 
    };


    Widget.CallbackWidget.prototype.toggle = function() {    
        this.closed = !this.closed;
        
        this.controls.panel.classList.toggle("closed");
        this.controls.fab.classList.toggle("closed");

        if (!this.closed) {
            //after animation set back the visibility .3s
            var self = this;
            setTimeout(function() {
                self.controls.name.focus();
            },300);
        }        
    }
    
    Widget.CallbackWidget.prototype.call = function() {                
        if (!this.validateForm())
            return;
        
        var callbackRequestData = new Widget.CallBackRequest(
            this.controls.name, 
            this.controls.phone,
            this.controls.interest,
            this.controls.email
        )

        this.showSpinner();
        
        var self = this;
    
        Widget.callService.sendCallbackRequest(callbackRequestData, 
            function(message) {
                self.hideSpinner();
                self.message.show(message, 
                    self.message.VISUAL_SUCCESS, 
                    self.message.DURATION_LONG);                    
            }, 
            function(error) {
                self.hideSpinner();
                self.message.show(error, 
                    self.message.VISUAL_FAILURE, 
                    self.message.DURATION_LONG);                        
            });                    
    }

    Widget.CallbackWidget.prototype.validateForm = function() {
        var formCallBack = this.container.getElementsByClassName("form-call-back")[0];
        var fields = formCallBack.getElementsByClassName("widget__field");
        var formValidationResult = true;

        for (let i = 0; i < fields.length; i++) {
            var field = fields[i];
            var result = Widget.validations.validateFinal(field);

            if (result != null) {
                Widget.validations.showFieldMessage(field, result);
                formValidationResult == true?field.focus():null;
                formValidationResult = false;
            }
            else {
                Widget.validations.hideFieldMessage(field);
            }
        }

        return formValidationResult;
    }

    Widget.CallbackWidget.prototype.showSpinner = function() {
        this.controls.spinnerCall.style.display = "block";
        this.controls.callButton.disabled = "disabled";
    }
    
    Widget.CallbackWidget.prototype.hideSpinner = function() {
        this.controls.spinnerCall.style.display = "none";
        this.controls.callButton.disabled = null;
    }

    /*
     * Widget Message
     */
    Widget.Message = function(message, messageHolder) {
        this.lastTimeout = null;
        this.message = message;
        this.messageHolder = messageHolder;
    }

    Widget.Message.prototype.show = function(message, type, duration) {
        type = type?type:this.VISUAL_SUCCESS;
        duration = duration?duration:this.DURATION_SHORT;

        this.message.innerText = message;
        this.messageHolder.classList.add("open");

        if (type == this.VISUAL_FAILURE)
            this.messageHolder.classList.add("error");

        if (this.lastTimeout != null)
            clearTimeout(this.lastTimeout);

        var self = this;
        this.lastTimeout = setTimeout(function(){ 
            self.close();
        }, duration)
    }

    Widget.Message.prototype.close = function() {
        this.messageHolder.classList.remove("open");
        this.messageHolder.classList.remove("error");
    }

    Widget.Message.prototype.VISUAL_SUCCESS = 1;
    Widget.Message.prototype.VISUAL_FAILURE = 2;
    Widget.Message.prototype.DURATION_SHORT = 2500;
    Widget.Message.prototype.DURATION_LONG = 5000;
    
    /*
     * CallBackRequest model
     */
    Widget.CallBackRequest = function (name, phone, interest, email) {
        this.name = name;
        this.phone = phone;
        this.interest = interest;
        this.email = email;
    }

    /*
     * callBack service
     * Suggestion: use webpack to separate it all into N files
     */
    Widget.callService = {};
    Widget.callService.url = "";
    Widget.callService.ERROR_MESSAGE = "Sorry, your request couldn't be complete"; 
    Widget.callService.sendCallbackRequest = function(data, success, failure) {
        var http = new XMLHttpRequest();
        var route = '/call';        

        setTimeout(function() {//This line is just to emulate a 3000ms response time, so we can visualize the spinner
            try {
                http.open("POST", Widget.callService.url + route, true);
                http.setRequestHeader('Content-Type', 'application/json');
    
    
                http.onload = function() {
                    console.log(this);
    
                    if (http.status == 200)
                        success("Success!!!");
                    else 
                        failure(Widget.callService.ERROR_MESSAGE);
                }
    
                http.onerror = function(error) {
                    console.log(error);
                    console.log(this);
                    failure(Widget.callService.ERROR_MESSAGE);
                }
                
                http.send(JSON.stringify(data));  
            } catch (error) {
                console.log(error);
                failure(Widget.callService.ERROR_MESSAGE);
            }          
        },3000);//This line is just to emulate a 3000ms response time, so we can visualize the spinner
    }

    /*
     * validations
     */
    Widget.validations = {}
    Widget.validations.setListeners = function(element) {
        var fields = element.getElementsByClassName("widget__field");
        
        for (let i = 0; i < fields.length; i++) {
            var field = fields[i];
            field.addEventListener("blur", this.onblur);
            field.addEventListener("keypress", this.onkeypress);
            field.addEventListener("change", this.onchange);
        }
    };

    Widget.validations.validateFinal = function(control) {
        var result;

        if (control.hasAttribute("required")) { 
            result = Widget.validations.required(control); /**/
            if (result != null) return result;
        }

        if (control.tagName.toUpperCase() == "INPUT") {
            switch (control.dataset.validation) {
                case "email":           
                    result = Widget.validations.email(control);
                    if (result != null) return result;         
                    break;
                case "phone":
                    result = Widget.validations.phone(control); 
                    if (result != null) return result;     
                    break;      
                default:
                    break;
            }
        }

        return null;
    }

    Widget.validations.onblur = function(e) {        
        var result = Widget.validations.validateFinal(e.target);

        if (result != null && e.target.classList.contains("dirty"))
            Widget.validations.showFieldMessage(e.target, result); 
        else
            Widget.validations.hideFieldMessage(e.target);
    }

    Widget.validations.onchange = function(e) {
        if (!e.target.classList.contains("dirty"))
            e.target.classList.add("dirty");
    }

    /*
     * keypress validators
     */
    Widget.validations.onkeypress = function(e) {
        Widget.validations.hideFieldMessage(e.target); 
        
        if (e.target.tagName.toUpperCase() == "INPUT") {
            switch (e.target.dataset.validation) {
                case "phone":
                    Widget.validations.onkeypressNumeric(e);
                    break;      
                default:
                    break;
            }
        }
    }

    Widget.validations.onkeypressNumeric = function(e) {
        var keycode = event.keyCode ? event.keyCode : event.which;

        if (!(event.shiftKey == false && 
            (keycode == 32 || keycode == 43 || keycode == 45 || keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || 
            (keycode >= 48 && keycode <= 57)))) {
                console.log(keycode);
            event.preventDefault();
        }
    }

    /*
     * final validators
     */

    Widget.validations.required = function(control) {
        if (control.value.trim().length == 0)
            return "Cannot be empty";
        else 
            return null;
    }
    
    Widget.validations.email = function(control) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!re.test(String(control.value).toLowerCase()))
            return "Invalid e-mail";
        else 
            return null;
    }
    
    Widget.validations.phone = function(control) {
        var re = /^[^a-zA-Z]+$/;

        if (!re.test(String(control.value).toLowerCase()) || control.value.length < 8)
            return "Invalid phone";
        else 
            return null;
    }

    /*
     * validation field messages
     */
    Widget.validations.showFieldMessage = function(control, result) {
        var validationMessage = control.parentElement.querySelectorAll(".validation-message[data-for='" + control.name + "']")[0];

        if (validationMessage != null) {
            validationMessage.style.visibility = "visible"; 
            validationMessage.innerText = result;
        }

        control.classList.add("invalid-field");
    }
    
    Widget.validations.hideFieldMessage = function(control) {
        var validationMessage = control.parentElement.querySelectorAll(".validation-message[data-for='" + control.name + "']")[0];
        
        if (validationMessage != null)
            validationMessage.style.visibility = "hidden";

        control.classList.remove("invalid-field");
    }

})();